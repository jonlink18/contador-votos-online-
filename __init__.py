from flask import Flask, redirect, url_for, render_template, request, session, flash


app = Flask(__name__)
app.secret_key = 'AjqXMpOu4T'

@app.route('/')
def iniciar():
    if 'voto' in session:
        return redirect(url_for('arrancar'))
    return render_template('voto.html')

@app.route('/dvotope/')
def dvotope():
    petra = open("petra.txt", '+a')
    petra.write("1\n")
    petra.close()

    return render_template('dvotope.html')

@app.route('/dvotopd/')
def dvotopd():
    pedro = open("pedro.txt", '+a')
    pedro.write("1\n")
    pedro.close()
    return render_template('dvotopd.html')

@app.route('/rvoto/')
def rvoto():
    vopt=0
    vopr=0
    vopr = sum(1 for line in open('pedro.txt'))
    vopt = sum(1 for line in open('petra.txt'))
    return render_template('rvoto.html',vop=vopt,vopd= vopr)

@app.route('/voto/')
def voto():
    return render_template('voto.html')

if __name__ == '__main__':
    app.run()

